package just.innovates.workers.newrequest.request;

import com.google.gson.annotations.SerializedName;

import just.innovates.workers.base.DefaultRequest;

public class NewOrderRequest extends DefaultRequest {

    @SerializedName("user_id")
    int userId;

    @SerializedName("name")
    String title;

    @SerializedName("details")
    String details;


    @SerializedName("lat")
    double lat=0.0;


    @SerializedName("lng")
    double lng=0.0;


    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getDetails() {
        return details;
    }

    public String getTitle() {
        return title;
    }
}
