package just.innovates.workers.newrequest;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import just.innovates.workers.R;
import just.innovates.workers.base.DefaultResponse;
import just.innovates.workers.base.FragmentHelper;
import just.innovates.workers.base.SelectLocationActivity;
import just.innovates.workers.base.UserPreferenceHelper;
import just.innovates.workers.base.constantsutils.Codes;
import just.innovates.workers.base.filesutils.FileOperations;
import just.innovates.workers.base.filesutils.VolleyFileObject;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;
import just.innovates.workers.home.HomeFragment;
import just.innovates.workers.newrequest.request.NewOrderRequest;


public class AddServiceFragment extends Fragment {

    View view;

    ArrayList<VolleyFileObject> volleyFileObjects;
    RelativeLayout relativeLayoutImage;
    EditText title, details, location;
    ImageView requestImage;

    NewOrderRequest newOrderRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_request, container, false);
        ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        init();
        setClicks();
        return view;
    }


    private void init() {
        relativeLayoutImage = view.findViewById(R.id.rl_new_request_image);
        title = view.findViewById(R.id.et_new_request_title);
        details = view.findViewById(R.id.et_new_request_details);
        location = view.findViewById(R.id.et_new_request_location);
        requestImage = view.findViewById(R.id.iv_new_request_image);
        volleyFileObjects = new ArrayList<>();
        newOrderRequest = new NewOrderRequest();
    }

    private void setClicks() {

        relativeLayoutImage.setOnClickListener(view -> {
            String[]choiceString = new String[]{"Gallery", "Camera"};
            AlertDialog.Builder dialog = new AlertDialog.Builder(requireActivity());
            dialog.setIcon(R.mipmap.ic_launcher);
            dialog.setTitle("Select From");
            dialog.setItems(choiceString,
                    (dialog1, which) -> {
                        Intent intent;
                        if (which == 0) {
                            intent = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        } else {
                            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                       startActivityForResult(Intent.createChooser(intent, "Select picture"), Codes.FILE_TYPE_IMAGE);
                    }).show();
        });

        location.setOnClickListener(view -> {
            Intent intent = new Intent(requireActivity(), SelectLocationActivity.class);
            startActivityForResult(intent, 1500);
        });


        view.findViewById(R.id.btn_new_request_send).setOnClickListener(view -> sendNewOrderToServer());
    }


    private void sendNewOrderToServer() {

        newOrderRequest.setTitle(title.getText().toString());
        newOrderRequest.setDetails(details.getText().toString());
        newOrderRequest.setUserId(UserPreferenceHelper.getUserDetails().getId());


        newOrderRequest.setMethod("new_order");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                FragmentHelper.popAllFragments(requireActivity());
                FragmentHelper.addFragment(requireActivity(), new HomeFragment(), "HomeFragment");
                Toast.makeText(requireActivity(), "Order Sent Successfully Follow your order from orders list", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRequestError(Object error) {


            }
        }).multiPartConnect("", newOrderRequest, volleyFileObjects, DefaultResponse.class);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1500) {
            try {
                newOrderRequest.setLat(data.getDoubleExtra("lat", 0.0));
                newOrderRequest.setLng(data.getDoubleExtra("lng", 0.0));
                location.setText("Location Selected");
            } catch (Exception e) {
                e.getStackTrace();
            }
        } else {

                VolleyFileObject volleyFileObject =
                        FileOperations.getVolleyFileObject(getActivity(), data, "image",
                                Codes.FILE_TYPE_IMAGE);

                requestImage.setImageBitmap(volleyFileObject.getCompressObject().getImage());

                if (volleyFileObjects == null) {
                    volleyFileObjects = new ArrayList<>();
                }

                volleyFileObjects.add(volleyFileObject);


        }
    }


}