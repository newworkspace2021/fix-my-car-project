package just.innovates.workers.auth.response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("user")
	private UserItem user;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setUser(UserItem user){
		this.user = user;
	}

	public UserItem getUser(){
		return user;
	}
}