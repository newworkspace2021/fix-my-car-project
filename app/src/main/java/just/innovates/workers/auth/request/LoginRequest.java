package just.innovates.workers.auth.request;

import com.google.gson.annotations.SerializedName;

import just.innovates.workers.base.DefaultRequest;

public class LoginRequest extends DefaultRequest {

	@SerializedName("password")
	private String password;
	@SerializedName("phone")
	private String phone;

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}


}