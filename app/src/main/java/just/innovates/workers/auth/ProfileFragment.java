package just.innovates.workers.auth;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import just.innovates.workers.R;
import just.innovates.workers.auth.request.RegisterRequest;
import just.innovates.workers.auth.request.UpdateProfileRequest;
import just.innovates.workers.auth.response.LoginResponse;
import just.innovates.workers.auth.response.UserItem;
import just.innovates.workers.base.HomeActivity;
import just.innovates.workers.base.UserPreferenceHelper;
import just.innovates.workers.base.Validate;
import just.innovates.workers.base.constantsutils.Codes;
import just.innovates.workers.base.filesutils.FileOperations;
import just.innovates.workers.base.filesutils.VolleyFileObject;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;

public class ProfileFragment extends Fragment {

    EditText name, phone,email, password,confirmPassword;
    ImageView profileImage;
    ArrayList<VolleyFileObject> volleyFileObjects;
    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        name = view.findViewById(R.id.et_register_name);
        phone = view.findViewById(R.id.et_register_phone);
        email = view.findViewById(R.id.et_register_email);
        password = view.findViewById(R.id.et_register_password);
        confirmPassword = view.findViewById(R.id.et_register_confirm_password);
        profileImage = view.findViewById(R.id.iv_profile_image);
        setClicks();
        setUserDetails();

        return view;


    }

    private void setUserDetails(){
        UserItem userItem = UserPreferenceHelper.getUserDetails();
        ConnectionHelper.loadImage(profileImage,userItem.getImage());
        name.setText(userItem.getName());
        phone.setText(userItem.getPhone());
        email.setText(userItem.getEmail());

    }


    private void registerDataToServer() {

        if (Validate.isEmpty(name.getText().toString())) {
            name.setError("Enter Name");
        } else if (Validate.isEmpty(phone.getText().toString())) {
            phone.setError("Enter Phone");
        } else if (Validate.isEmpty(email.getText().toString())) {
            email.setError("Enter Email");
        } else if (!Validate.isMail(email.getText().toString())) {
            Toast.makeText(requireActivity(), "Not Valid Email", Toast.LENGTH_SHORT).show();
        }
        else if (Validate.isEmpty(password.getText().toString())) {
            password.setError("Enter Password");
        }else if (Validate.isEmpty(confirmPassword.getText().toString())) {
            confirmPassword.setError("Enter Confirm Password");
        }else if (!Validate.isEqual(password.getText().toString(),confirmPassword.getText().toString())) {
            Toast.makeText(requireActivity(), "Sorry! Passwords not matches", Toast.LENGTH_SHORT).show();
        }
        else {

            UpdateProfileRequest loginRequest = new UpdateProfileRequest();
            loginRequest.setMethod("update_profile");
            loginRequest.setPhone(phone.getText().toString());
            loginRequest.setEmail(email.getText().toString());
            loginRequest.setName(name.getText().toString());
            loginRequest.setPassword(password.getText().toString());
            loginRequest.setUserId(UserPreferenceHelper.getUserDetails().getId()+"");


            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    LoginResponse loginResponse = (LoginResponse) response;

                    if (loginResponse.getState() == 101) {
                        requireActivity().finishAffinity();
                        UserPreferenceHelper.saveUserDetails(loginResponse.getUser());
                        startActivity(new Intent(requireActivity(), HomeActivity.class));
                    }

                }

                @Override
                public void onRequestError(Object error) {
                }
            }).multiPartConnect("",loginRequest,volleyFileObjects, LoginResponse.class);
        }

    }

    private void setClicks(){
        view.findViewById(R.id.btn_update).setOnClickListener(view -> registerDataToServer());

        profileImage.setOnClickListener(view -> {
            String[]choiceString = new String[]{"Gallery", "Camera"};
            AlertDialog.Builder dialog = new AlertDialog.Builder(requireActivity());
            dialog.setIcon(R.mipmap.ic_launcher);
            dialog.setTitle("Select From");
            dialog.setItems(choiceString,
                    (dialog1, which) -> {
                        Intent intent;
                        if (which == 0) {
                            intent = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        } else {
                            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                        startActivityForResult(Intent.createChooser(intent, "Select picture"), Codes.FILE_TYPE_IMAGE);
                    }).show();
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            Codes.FILE_TYPE_IMAGE);

         profileImage.setImageBitmap(volleyFileObject.getCompressObject().getImage());

            if (volleyFileObjects == null) {
                volleyFileObjects = new ArrayList<>();
            }

            volleyFileObjects.add(volleyFileObject);

    }


}