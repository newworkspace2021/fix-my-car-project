package just.innovates.workers.auth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import just.innovates.workers.R;
import just.innovates.workers.auth.request.RegisterRequest;
import just.innovates.workers.auth.response.LoginResponse;
import just.innovates.workers.base.UserPreferenceHelper;
import just.innovates.workers.base.Validate;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;
import just.innovates.workers.base.HomeActivity;

public class RegisterActivity extends AppCompatActivity {

    EditText name, phone,email, password,confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_register);

        name = findViewById(R.id.et_register_name);
        phone = findViewById(R.id.et_register_phone);

        email = findViewById(R.id.et_register_email);
        password = findViewById(R.id.et_register_password);
        confirmPassword = findViewById(R.id.et_register_confirm_password);


        setClicks();
    }


    private void registerDataToServer() {

        if (Validate.isEmpty(name.getText().toString())) {
            name.setError("Enter Name");
        } else if (Validate.isEmpty(phone.getText().toString())) {
            phone.setError("Enter Phone");
        } else if (Validate.isEmpty(email.getText().toString())) {
            email.setError("Enter Email");
        } else if (!Validate.isMail(email.getText().toString())) {
            Toast.makeText(this, "Not Valid Email", Toast.LENGTH_SHORT).show();
        }
        else if (Validate.isEmpty(password.getText().toString())) {
            password.setError("Enter Password");
        }else if (Validate.isEmpty(confirmPassword.getText().toString())) {
            confirmPassword.setError("Enter Confirm Password");
        }else if (!Validate.isEqual(password.getText().toString(),confirmPassword.getText().toString())) {
            Toast.makeText(this, "Sorry! Passwords not matches", Toast.LENGTH_SHORT).show();
        }
        else {

            RegisterRequest loginRequest = new RegisterRequest();
            loginRequest.setMethod("register");
            loginRequest.setPhone(phone.getText().toString());
            loginRequest.setEmail(email.getText().toString());
            loginRequest.setName(name.getText().toString());
            loginRequest.setPassword(password.getText().toString());


            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    LoginResponse loginResponse = (LoginResponse) response;

                    if (loginResponse.getState() == 101) {
                        UserPreferenceHelper.saveUserDetails(loginResponse.getUser());
                        startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
                    } else {
                        Toast.makeText(RegisterActivity.this, "Sorry entered email or phone is used before", Toast.LENGTH_SHORT).show();
                     }

                }

                @Override
                public void onRequestError(Object error) {
                }
            }).requestJsonObject(loginRequest, LoginResponse.class);
        }

    }

    private void setClicks(){
        findViewById(R.id.btn_register).setOnClickListener(view -> registerDataToServer());
    }
}
