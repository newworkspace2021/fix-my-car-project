package just.innovates.workers.auth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import just.innovates.workers.R;
import just.innovates.workers.auth.request.LoginRequest;
import just.innovates.workers.auth.response.LoginResponse;
import just.innovates.workers.base.UserPreferenceHelper;
import just.innovates.workers.base.Validate;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;
import just.innovates.workers.base.HomeActivity;

public class LoginActivity extends AppCompatActivity {

    EditText phone, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        phone = findViewById(R.id.et_phone);
        password = findViewById(R.id.et_password);

        setClicks();

    }

    private void loginFromServer() {

        if (Validate.isEmpty(phone.getText().toString())) {
            phone.setError("Enter Phone");
        } else if (Validate.isEmpty(password.getText().toString())) {
            password.setError("Enter Password");
        } else {

            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setMethod("login");
            loginRequest.setPhone(phone.getText().toString());
            loginRequest.setPassword(password.getText().toString());


            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    LoginResponse loginResponse = (LoginResponse) response;

                    if (loginResponse.getState() == 101) {
                        UserPreferenceHelper.saveUserDetails(loginResponse.getUser());
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    } else {
                        Toast.makeText(LoginActivity.this, "Sorry your password or phone not correct", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onRequestError(Object error) {
                }
            }).requestJsonObject(loginRequest, LoginResponse.class);
        }

    }


    private void setClicks() {

        findViewById(R.id.login).setOnClickListener(view -> loginFromServer());

        findViewById(R.id.register).setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));
    }
}
