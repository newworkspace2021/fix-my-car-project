package just.innovates.workers.auth.request;

import com.google.gson.annotations.SerializedName;

import just.innovates.workers.base.DefaultRequest;

public class UpdateProfileRequest extends DefaultRequest {


	@SerializedName("password")
	private String password;


	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;


	@SerializedName("email")
	private String email;


	@SerializedName("user_id")
	private String userId;




	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}



	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}
}