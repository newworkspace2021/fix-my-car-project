package just.innovates.workers.home.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class HomeResponse{

	@SerializedName("ads")
	private List<AdsItem> ads;

	@SerializedName("state")
	private int state;

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	public void setAds(List<AdsItem> ads){
		this.ads = ads;
	}

	public List<AdsItem> getAds(){
		return ads;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}
}