package just.innovates.workers.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.List;

import just.innovates.workers.base.DefaultRequest;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;
import just.innovates.workers.home.response.AdsItem;
import just.innovates.workers.home.response.HomeResponse;
import just.innovates.workers.R;

public class HomeFragment extends Fragment {


    View view;
    HomeCategoriesAdapter homeCategoriesAdapter;
    SliderLayout slider;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_2, container, false);

        slider = view.findViewById(R.id.slider);

        getHomeDataFromServer();

        return view;
    }




    private void getHomeDataFromServer(){
        DefaultRequest defaultRequest = new DefaultRequest();
        defaultRequest.setMethod("home");
        new ConnectionHelper(new ConnectionListener(){
            @Override
            public void onRequestSuccess(Object response){

                HomeResponse homeResponse = (HomeResponse)response;

                homeCategoriesAdapter = new HomeCategoriesAdapter(homeResponse.getCategories());

                requireActivity().runOnUiThread(() -> {
                    RecyclerView categoriesList = view.findViewById(R.id.categoriesList);
                    categoriesList.setAdapter(homeCategoriesAdapter);
                    setAdsSlider(homeResponse.getAds());
                });


            }

            @Override
            public void onRequestError(Object error) {


            }
        }).requestJsonObject(defaultRequest, HomeResponse.class);

    }



    private void setAdsSlider(List<AdsItem> ads){

        for (int i=0;i<ads.size();i++){
            int finalI = i ;
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .description(ads.get(i).getTitle())
                    .image(ads.get(i).getImage())
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(slider -> {
                        try {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ads.get(finalI).getLink()));
                            startActivity(browserIntent);
                        }catch (Exception e){
                            e.getStackTrace();
                        }
                    });
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(4000);
    }



}