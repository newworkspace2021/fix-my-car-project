package just.innovates.workers.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;

import java.util.List;

import just.innovates.workers.R;
import just.innovates.workers.companies.CompaniesFragment;
import just.innovates.workers.home.response.CategoriesItem;
import just.innovates.workers.base.FragmentHelper;


public class HomeCategoriesAdapter extends RecyclerView.Adapter<HomeCategoriesAdapter.ViewHolder> {


    List<CategoriesItem> categoryModels;


    public HomeCategoriesAdapter( List<CategoriesItem> companyModels) {

        this.categoryModels = companyModels;

    }


    @Override
    public int getItemCount() {
        return categoryModels == null ? 0 : categoryModels  .size();
    }


    @Override
    public HomeCategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_category_2, parent, false);
        HomeCategoriesAdapter.ViewHolder viewHolder = new HomeCategoriesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final HomeCategoriesAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.itemView.getContext()).load(categoryModels.get(position).getImage()).into(holder.imageView);
        holder.name.setText(categoryModels.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("category_id",categoryModels.get(position).getId());
                CompaniesFragment companiesFragment = new CompaniesFragment();
                companiesFragment.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),companiesFragment,"CompaniesFragment");
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
        }
    }
}