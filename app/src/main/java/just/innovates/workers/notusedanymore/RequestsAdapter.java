package just.innovates.workers.notusedanymore;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import just.innovates.workers.R;


public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;



    public RequestsAdapter(OnItemClickListener onItemClickListener ) {
        this.onItemClickListener = onItemClickListener;


    }


    @Override
    public int getItemCount() {
        return 20;
    }


    @Override
    public RequestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request, parent, false);
        RequestsAdapter.ViewHolder viewHolder = new RequestsAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final RequestsAdapter.ViewHolder holder, final int position) {


    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;


        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);



        }
    }
}