package just.innovates.workers.companies.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LocationsResponse{

	@SerializedName("locations")
	private List<LocationsItem> locations;

	@SerializedName("state")
	private int state;

	public void setLocations(List<LocationsItem> locations){
		this.locations = locations;
	}

	public List<LocationsItem> getLocations(){
		return locations;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}
}