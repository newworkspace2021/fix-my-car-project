package just.innovates.workers.companies;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import just.innovates.workers.R;
import just.innovates.workers.companies.response.LocationsItem;
import just.innovates.workers.notusedanymore.OnItemClickListener;


public class CompaniesAdapter extends RecyclerView.Adapter<CompaniesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    List<LocationsItem> companyModels;


    public CompaniesAdapter(OnItemClickListener onItemClickListener, List<LocationsItem> companyModels) {
        this.onItemClickListener = onItemClickListener;
        this.companyModels = companyModels;

    }


    @Override
    public int getItemCount() {
        return companyModels == null ? 0 : companyModels.size();
    }


    @Override
    public CompaniesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_company, parent, false);
        CompaniesAdapter.ViewHolder viewHolder = new CompaniesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final CompaniesAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.itemView.getContext()).load(companyModels.get(position).getImage()).into(holder.image);

        holder.name.setText(companyModels.get(position).getName());
        holder.address.setText(companyModels.get(position).getAddress());
        holder.times.setText("Works From : "+companyModels.get(position).getStartAt()+" To : "+companyModels.get(position).getEndAt());
        holder.call.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + companyModels.get(position).getPhone()));
            view.getContext().startActivity(intent);
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name,address,times,call;


        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.iv_company_photo);
            name = view.findViewById(R.id.tv_company_name);
            address = view.findViewById(R.id.tv_company_address);
            times = view.findViewById(R.id.tv_company_times);
            call = view.findViewById(R.id.tv_company_call);


        }
    }
}