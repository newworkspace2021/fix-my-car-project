package just.innovates.workers.companies.response;

import com.google.gson.annotations.SerializedName;

public class LocationsItem{

	@SerializedName("end_at")
	private String endAt;

	@SerializedName("image")
	private String image;

	@SerializedName("address")
	private String address;

	@SerializedName("category_id")
	private String categoryId;

	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("start at")
	private String startAt;

	@SerializedName("id")
	private int id;

	public void setEndAt(String endAt){
		this.endAt = endAt;
	}

	public String getEndAt(){
		return endAt;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setStartAt(String startAt){
		this.startAt = startAt;
	}

	public String getStartAt(){
		return startAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}