package just.innovates.workers.companies.request;

import com.google.gson.annotations.SerializedName;

import just.innovates.workers.base.DefaultRequest;

public class LocationsRequest extends DefaultRequest {

	@SerializedName("category_id")
	private int categoryId;

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}
}