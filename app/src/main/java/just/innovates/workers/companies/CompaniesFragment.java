package just.innovates.workers.companies;


import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.workers.R;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;
import just.innovates.workers.companies.request.LocationsRequest;
import just.innovates.workers.companies.response.LocationsResponse;
import just.innovates.workers.notusedanymore.OnItemClickListener;

public class CompaniesFragment extends Fragment {


    View v;
    CompaniesAdapter companiesAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requireActivity().requestPermissions(new String[]{Manifest.permission.CALL_PHONE},12);
        }
        getLocationsFromServer();

        return v;
    }



    private void getLocationsFromServer(){
        LocationsRequest defaultRequest = new LocationsRequest();
        defaultRequest.setMethod("locations");
        defaultRequest.setCategoryId(getArguments().getInt("category_id",0));
        new ConnectionHelper(new ConnectionListener(){
            @Override
            public void onRequestSuccess(Object response){

                LocationsResponse locationsResponse = (LocationsResponse)response;

                RecyclerView recyclerView = v.findViewById(R.id.categoriesList);

                companiesAdapter = new CompaniesAdapter(new OnItemClickListener() {
                    @Override
                    public void onItemClickListener(int position) {

                    }
                },locationsResponse.getLocations());

                recyclerView.setAdapter(companiesAdapter);


            }

            @Override
            public void onRequestError(Object error) {


            }
        }).requestJsonObject(defaultRequest, LocationsResponse.class);

    }






}