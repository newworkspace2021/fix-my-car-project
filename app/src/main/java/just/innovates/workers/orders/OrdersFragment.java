package just.innovates.workers.orders;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.workers.R;
import just.innovates.workers.base.UserPreferenceHelper;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;
import just.innovates.workers.companies.CompaniesAdapter;
import just.innovates.workers.companies.request.LocationsRequest;
import just.innovates.workers.companies.response.LocationsResponse;
import just.innovates.workers.notusedanymore.OnItemClickListener;
import just.innovates.workers.orders.request.OrdersRequest;
import just.innovates.workers.orders.response.OrdersResponse;

public class OrdersFragment extends Fragment {


    View v;
    OrdersAdapter ordersAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_orders, container, false);
        getOrdersFromServer();

        return v;
    }




    private void getOrdersFromServer(){

        OrdersRequest ordersRequest = new OrdersRequest();
        ordersRequest.setMethod("orders");
        ordersRequest.setUserId(UserPreferenceHelper.getUserDetails().getId());
        new ConnectionHelper(new ConnectionListener(){
            @Override
            public void onRequestSuccess(Object response){

                OrdersResponse ordersResponse = (OrdersResponse)response;

                RecyclerView ordersList = v.findViewById(R.id.ordersList);

                ordersAdapter = new OrdersAdapter(ordersResponse.getOrders());

                ordersList.setAdapter(ordersAdapter);


            }

            @Override
            public void onRequestError(Object error) {


            }
        }).requestJsonObject(ordersRequest, OrdersResponse.class);

    }




}