package just.innovates.workers.orders.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import just.innovates.workers.auth.response.UserItem;

public class OrdersItem{

	@SerializedName("image")
	private String image;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("rate")
	private String rate;

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("details")
	private String details;

	@SerializedName("comment")
	private String comment;

	@SerializedName("id")
	private int id;

	@SerializedName("state")
	private int state;



	@Expose
	private String stateString;

	@SerializedName("worker")
	private UserItem worker;

	@SerializedName("worker_id")
	private String workerId;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setRate(String rate){
		this.rate = rate;
	}

	public String getRate(){
		return rate;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setWorker(UserItem worker){
		this.worker = worker;
	}

	public UserItem getWorker(){
		return worker;
	}

	public void setWorkerId(String workerId){
		this.workerId = workerId;
	}

	public String getWorkerId(){
		return workerId;
	}


	public void setStateString(String stateString) {
		if(state==0){
			stateString = "Waiting For Acceptance";
		}else if(state==1){
			stateString = "Accepted";
		}else if(state==2){
			stateString = "On They Way";
		}else if(state==3){
			stateString = "In Progress";
		}else if(state==4){
			stateString = "Finished";
		}else if(state==5){
			stateString = "Rated";
		}
		this.stateString = stateString;
	}

	public String getStateString() {
		if(state==0){
			stateString = "Waiting For Acceptance";
		}else if(state==1){
			stateString = "Accepted";
		}else if(state==2){
			stateString = "On They Way";
		}else if(state==3){
			stateString = "In Progress";
		}else if(state==4){
			stateString = "Finished";
		}else if(state==5){
			stateString = "Rated";
		}
		return stateString;
	}
}