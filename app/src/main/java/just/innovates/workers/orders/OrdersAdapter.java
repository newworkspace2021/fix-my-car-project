package just.innovates.workers.orders;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import just.innovates.workers.R;
import just.innovates.workers.base.DefaultResponse;
import just.innovates.workers.base.constantsutils.Codes;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.base.volleyutils.ConnectionListener;
import just.innovates.workers.notusedanymore.OnItemClickListener;
import just.innovates.workers.orders.request.RateOrderRequest;
import just.innovates.workers.orders.response.OrdersItem;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    List<OrdersItem> ordersItemList;

    public OrdersAdapter(List<OrdersItem> ordersItemList) {
        this.ordersItemList=ordersItemList;
    }


    @Override
    public int getItemCount() {
        return ordersItemList.size();
    }


    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        OrdersAdapter.ViewHolder viewHolder = new OrdersAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final OrdersAdapter.ViewHolder holder, final int position) {
        Picasso.with(holder.itemView.getContext()).load(ordersItemList.get(position).getWorker().getImage()).into(holder.workerImage);



        holder.id.setText("#"+ordersItemList.get(position).getId()+" ( "+ordersItemList.get(position).getStateString()+" )");

        holder.title.setText(ordersItemList.get(position).getName());
        holder.details.setText(ordersItemList.get(position).getDetails());

        holder.workerName.setText(ordersItemList.get(position).getWorker().getName());
        holder.workerOffer.setText("Offer : "+ordersItemList.get(position).getPrice()+" EGP");

        if(ordersItemList.get(position).getState()==4){
            holder.workerCall.setText("Rate");
            holder.workerCall.setVisibility(View.VISIBLE);
        }else if(ordersItemList.get(position).getState()==5){
            holder.workerCall.setVisibility(View.GONE);
        }else {
            holder.workerCall.setText("Call");
            holder.workerCall.setVisibility(View.VISIBLE);
        }


        holder.workerCall.setOnClickListener(view -> {
            if(ordersItemList.get(position).getState()==4){
                rateBarberDialog(view.getContext(),ordersItemList.get(position).getId());
            }else {
                try {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ordersItemList.get(position).getWorker().getPhone()));
                    view.getContext().startActivity(intent);
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        });


    }


    private void rateBarberDialog(Context context,int id){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater2 =  ((Activity)context).getLayoutInflater();
        View dialogView = inflater2.inflate(R.layout.dialog_rate_worker, null);
        dialogBuilder.setView(dialogView);
        EditText commentEditText = dialogView.findViewById(R.id.et_rate_barber_comment);
        MaterialRatingBar ratingRateBar = dialogView.findViewById(R.id.rb_rating_barber_rate);
        TextView textView = dialogView.findViewById(R.id.tv_rate_barber_confirm);
        AlertDialog alertDialog = dialogBuilder.create();
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
                alertDialog.dismiss();
                sendRatingToServer(commentEditText.getText()+"",ratingRateBar.getRating()+"",id);
            }
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.show();
    }


    public void sendRatingToServer(String comment,String rate,int id){

        RateOrderRequest rateOrderRequest = new RateOrderRequest();
        rateOrderRequest.setMethod("rate_order");
        rateOrderRequest.setOrderId(id);
        rateOrderRequest.setComment(comment);
        rateOrderRequest.setRate(""+rate);

        new ConnectionHelper(new ConnectionListener(){

            @Override
            public void onRequestSuccess(Object response) {

            }

            @Override
            public void onRequestError(Object error) {


            }
        }).requestJsonObject(rateOrderRequest, DefaultResponse.class);
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView workerImage;
        TextView id, title,details,workerName,workerOffer,workerCall;


        public ViewHolder(View view) {
            super(view);
            id = view.findViewById(R.id.tv_order_id);
            workerImage = view.findViewById(R.id.iv_order_worker_image);
            title = view.findViewById(R.id.tv_order_title);
            details = view.findViewById(R.id.tv_order_details);
            workerName = view.findViewById(R.id.tv_order_worker_name);
            workerOffer = view.findViewById(R.id.tv_order_worker_price);
            workerCall = view.findViewById(R.id.tv_order_call_worker);;


        }
    }
}