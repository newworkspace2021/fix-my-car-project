package just.innovates.workers.orders.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrdersResponse{

	@SerializedName("orders")
	private List<OrdersItem> orders;

	@SerializedName("state")
	private int state;

	public void setOrders(List<OrdersItem> orders){
		this.orders = orders;
	}

	public List<OrdersItem> getOrders(){
		return orders;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}
}