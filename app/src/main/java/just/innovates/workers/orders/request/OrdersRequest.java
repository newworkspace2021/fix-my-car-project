package just.innovates.workers.orders.request;

import com.google.gson.annotations.SerializedName;

import just.innovates.workers.base.DefaultRequest;

public class OrdersRequest extends DefaultRequest {

	@SerializedName("user_id")
	private int userId=1;

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserId() {
		return userId;
	}
}