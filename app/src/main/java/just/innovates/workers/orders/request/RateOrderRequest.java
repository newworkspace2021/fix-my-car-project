package just.innovates.workers.orders.request;

import com.google.gson.annotations.SerializedName;
 import just.innovates.workers.base.DefaultRequest;

public class RateOrderRequest extends DefaultRequest {



	@SerializedName("order_id")
	private int orderId;

	@SerializedName("rating")
	private String rate;

	@SerializedName("comment")
	private String comment;





	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRate() {
		return rate;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getOrderId() {
		return orderId;
	}
}
