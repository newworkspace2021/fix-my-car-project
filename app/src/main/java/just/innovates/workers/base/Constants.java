package just.innovates.workers.base;

public class Constants {

    public static final String SUCCESS = "200";

    public static final int FAQCLICK = 113;

    public static final int SHOW_MESSAGE = 10;
    public static final int BOOK_SERVICE_PAGE = 41;
    public static final int ADD_TO_BASKET = 42;
    public static final int REMOVE_FROM_BASKET = 43;



    public static final String CATEGORIES_MODEL = "categories_model";
    public static final String SUB_CATEGORIES_MODEL = "sub_categories_model";
    public static final String SERVICE_MODEL = "service_model";
    public static final String LAB_MODEL = "lab_model";
    public static final String ORDER_MODEL = "order_model";
    public static final String PRODUCT_MODEL = "product_model";





}
