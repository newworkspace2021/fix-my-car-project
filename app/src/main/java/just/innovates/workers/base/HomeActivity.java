package just.innovates.workers.base;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import io.github.jitinsharma.bottomnavbar.BottomNavBar;
import io.github.jitinsharma.bottomnavbar.model.NavObject;
import just.innovates.workers.R;
import just.innovates.workers.auth.ProfileFragment;
import just.innovates.workers.auth.response.UserItem;
import just.innovates.workers.base.volleyutils.ConnectionHelper;
import just.innovates.workers.home.HomeFragment;
import just.innovates.workers.newrequest.AddServiceFragment;
import just.innovates.workers.notusedanymore.AboutUsFragment;
import just.innovates.workers.orders.OrdersFragment;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView;
      DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE,Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 134);
        }

            drawer = findViewById(R.id.drawer_layout);
            navigationView = findViewById(R.id.nav_view);

            setNavigation();
            setupBottomMenu();

        FragmentHelper.addFragment(this, new HomeFragment(), "HomeFragment");
    }

    private void setNavigation(){
       ImageView userPhoto = navigationView.getHeaderView(0).findViewById(R.id.iv_header_photo);
       TextView name = navigationView.getHeaderView(0).findViewById(R.id.tv_header_name);
        TextView phone = navigationView.getHeaderView(0).findViewById(R.id.tv_header_phone);


       UserItem userItem = UserPreferenceHelper.getUserDetails();

        ConnectionHelper.loadImage(userPhoto,userItem.getImage());
        name.setText(userItem.getName());
        phone.setText(userItem.getPhone());

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            drawer.closeDrawers();
            if (menuItem.getItemId() == R.id.nav_about_us) {
                FragmentHelper.addFragment(HomeActivity.this, new AboutUsFragment(), "AboutUsFragment");
            } else if (menuItem.getItemId() == R.id.nav_rate_us) {
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                }
            } else if (menuItem.getItemId() == R.id.nav_share) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=com.google.android.apps.plus");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }else if(menuItem.getItemId() == R.id.nav_log_out){
                UserPreferenceHelper.clearUserDetails();
                finish();
            }

            return false;
        });
    }

    private void setupBottomMenu(){

        NavObject navObject = new NavObject("Home",getResources().getDrawable(R.drawable.ic_homes));
        NavObject navObject2 = new NavObject("Orders",getResources().getDrawable(R.drawable.ic_orderz));
        NavObject navObject3 = new NavObject("About",getResources().getDrawable(R.drawable.ic_abouts));
        NavObject navObject4 = new NavObject("Profile",getResources().getDrawable(R.drawable.ic_user));
        NavObject centerObject = new NavObject("Request",getResources().getDrawable(R.drawable.ic_request));


        ArrayList<NavObject> navObjects = new ArrayList<>();
        navObjects.add(navObject);
        navObjects.add(navObject2);
        navObjects.add(navObject3);
        navObjects.add(navObject4);


        BottomNavBar bottomNavigation = findViewById(R.id.bottomNavigation);


        bottomNavigation.init(centerObject, navObjects, (integer, aBoolean) -> {
            FragmentHelper.popAllFragments(HomeActivity.this);
            if(integer == -1){
                FragmentHelper.addFragment(HomeActivity.this, new AddServiceFragment(), "AddServiceFragment");
            }else if (integer == 0) {
                FragmentHelper.addFragment(HomeActivity.this, new HomeFragment(), "HomeFragment");
            }else if (integer == 1) {
                FragmentHelper.addFragment(HomeActivity.this, new OrdersFragment(), "OrdersFragment");
            }else if (integer == 2) {
                FragmentHelper.addFragment(HomeActivity.this, new AboutUsFragment(), "AboutUsFragment");
            }else if (integer == 3) {
                FragmentHelper.addFragment(HomeActivity.this, new ProfileFragment(), "ProfileFragment");
            }

            return null;
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
