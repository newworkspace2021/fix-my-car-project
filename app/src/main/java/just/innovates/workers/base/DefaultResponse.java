package just.innovates.workers.base;

import com.google.gson.annotations.SerializedName;

public class DefaultResponse {

    @SerializedName("result")
    private String data;

    @SerializedName("message")
    private String message;

    @SerializedName("state")
    private String status;

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "SendMessageResponse{" +
                        "data = '" + data + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}