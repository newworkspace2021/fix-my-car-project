package just.innovates.workers.base.constantsutils;

public class Codes {

    //RequestsCodes
    public static int FILE_TYPE_IMAGE = 10;
    public static int FILE_TYPE_PDF = 12;


    //ACTIONS
    public static int REGISTER = 1 ;
    public static int LOGIN_SCREEN = 2;
    public static int HOME_PAGE = 3 ;
    public static int DIET_PAGE = 4 ;
    public static int MALE_DETAILS_PAGE = 5 ;
    public static int PRODUCTS_PAGE = 6 ;
    public static int PRODUCT_DETAILS_PAGE = 7 ;
    public static int SHARED = 8 ;
    public static int EVENTS_DETAILS = 9 ;
    public static int PROFILE=10;
    public static int PAYMENTS=11;
    public static int TRAINERS=12;
    public static int ABOUT=13;
    public static int COMPLAINTS=14;
    public static int CONTACT_US=15;
    public static int SHOW_MESSAGE=16;
    public static int PACKAGE_DETAIL_PAGE=17;
    public static int HISTORY_PAGE=18;
    public static int FREEZE_PAGE=19;
    public static int PROBLEMS_PAGE=20;
    public static int SEND_MESSAGE_PAGE=21;
    public static int SHOW_PROBLEMS_CLICK=22;
    public static int INVITED_PAGE=23;
    public static int NOTIFICATIONS_PAGE=24;




}
