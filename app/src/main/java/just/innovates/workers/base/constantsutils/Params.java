package just.innovates.workers.base.constantsutils;

public class Params {


    //IntentParams

    public static final String INTENT_PAGE = "page";
    public static final String INTENT_BUNDLE = "bundle";
    public static final String BUNDLE_ID = "id";
    public static final String BUNDLE_PRODUCT_DETAILS = "product_details";

    public static final String BUNDLE_SUBSCRIBES_LINK="subscribes_link";


}
